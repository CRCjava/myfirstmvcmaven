package com.mataroin.models;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Servlet implementation class Controlador
 */
@WebServlet("/Controlador")
public class Controlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(Controlador.class);
       
	protected Interesado interesado=null;
	protected IDao interesadoDao=null;
	
	protected String nombre=null;
	protected String apellidos=null;
	protected String empresa=null;
	protected String telefono=null;
	protected String email=null;
	protected String ciudad=null;
	protected String comentario=null;
	protected int filesInsertades=0;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controlador() {
        super();

        logger.debug("Controlador estem en el constructor");
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("entrem a doPost");
        
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
	    
		nombre=request.getParameter("nombre");
		apellidos=request.getParameter("apellidos");
		empresa=request.getParameter("empresa");
		telefono=request.getParameter("telefono");
		email=request.getParameter("email");
		ciudad=request.getParameter("ciudad");
		comentario=request.getParameter("comentario");
		
		if ((request.getParameter("nombre") == null) || 
			(request.getParameter("apellidos") == null) ||
			(request.getParameter("email") == null)){
			logger.error("if request.getParamete('nombre') == null");
			procesError(request, response);	
			logger.debug("despres de cridar processError");
		}else{
			logger.error("estem en el else del if request.getParamete('nombre') == null");
			interesado=crearInteresado();
			interesadoDao= new InteresadoDao();
			try{	
				logger.error("estem en el try");
				filesInsertades=interesadoDao.add(interesado);
				if (filesInsertades == 1){
					logger.debug("if filesInsertades=1");
					procesOk(request, response);
				}else {
					logger.error("else filesInsertades!=1, no ha fet l'insert");
					procesError(request, response);
				}
			}catch(SQLException ex){
				logger.debug("en el catch sqlException " + ex.getMessage());
				procesError(request, response);
			}		
		}		
	}
	protected void procesOk(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		logger.debug("entrem a procesOk");
		ServletContext context=getServletContext();
		RequestDispatcher rdispatcher=context.getRequestDispatcher("/MostrarSortida.jsp");
		rdispatcher.forward(request, response);		
	}
	protected void procesError(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		logger.debug("entrem a procesError");
		ServletContext context=getServletContext();
		RequestDispatcher rdispatcher=context.getRequestDispatcher("/MostrarSortidaError.jsp");
		rdispatcher.forward(request, response);		
	}
	protected Interesado crearInteresado(){
		logger.debug("entrem a crearInteresado");
		interesado=new Interesado();
		interesado.setNombre(nombre);
		interesado.setApellidos(apellidos);
		interesado.setCiudad(ciudad);
		interesado.setComentario(comentario);
		interesado.setEmail(email);
		interesado.setEmpresa(empresa);
		interesado.setTelefono(telefono);
		
		logger.debug("return Interesado");
		return interesado;
	}
}
