package com.mataroin.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class InteresadoDao implements IDao {
	static final Logger logger = LogManager.getLogger(InteresadoDao.class);
	static{
		try {
			logger.debug("en el inicialitzador class.forname");
			Class.forName("com.mysql.jdbc.Driver");		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("en el inicialitzador driver " + e.getMessage() );
		}
	}
	public int add(Interesado interesado) throws SQLException{
		logger.info("interesadoDao.add");
		int filesInsertades= 0;
		Connection connect=null;
		PreparedStatement preparedStatement=null;
		
		try{
			logger.debug("interesadoDao.add try");
			preparedStatement = null;
			connect = DriverManager
				      .getConnection("jdbc:mysql://localhost/mataroin?user=root");
		
			preparedStatement = connect.prepareStatement(
					"insert into interesados(nombre,apellidos,empresa,telefono,email,ciudad,comentario) values (?,?,?,?,?,?,?)");
			
			preparedStatement.setString(1, interesado.getNombre());
			preparedStatement.setString(2, interesado.getApellidos());
			preparedStatement.setString(3, interesado.getEmpresa());
			preparedStatement.setString(4, interesado.getTelefono());
			preparedStatement.setString(5, interesado.getEmail());
			preparedStatement.setString(6, interesado.getCiudad());
			preparedStatement.setString(7, interesado.getComentario());
			
			filesInsertades=preparedStatement.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
			logger.error("interesadoDao.add catch" + e.getMessage() );
		}finally{
			if (connect!=null) connect.close();
			if (preparedStatement!=null) preparedStatement.close();
			logger.debug("interesadoDao.add finally");
		}
	return filesInsertades;
	}
}
