<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
<h1>L'Interessat s'ha afegit correctament</h1>
	<ul>
		<li>Nom: <%= request.getParameter("nombre") %></li>
		<li>Cognoms: <%= request.getParameter("apellidos") %></li>
		<li>Empresa: <%= request.getParameter("empresa") %></li>
		<li>Telefon: <%= request.getParameter("telefono") %></li>
		<li>Correu electronic: <%= request.getParameter("email") %></li>
		<li>Ciutat: <%= request.getParameter("ciudad") %></li>
		<li>Comentari <%= request.getParameter("comentario") %></li>
	</ul>
</body>
</html>